(define (domain world-of-blocks)

  (:requirements
    :adl
  )

  (:predicates
    (on-top ?x ?y)
    (on-floor ?x)
    (clear ?x)
    (holding ?x)
    (armempty)
  )

  (:action pickup
    :parameters (?x)
    :precondition
    (and
      (clear ?x)
      (on-floor ?x)
      (armempty)
    )
    :effect
    (and
      (not (on-floor ?x))
      (not (armempty))
      (holding ?x)
    )
  )

  ; od³ó¿ klocek na pod³o¿e
  (:action putdown
    :parameters (?x)
    :precondition
    (and
;     (not(armempty))
      (clear ?x)
      (holding ?x)
    )
    :effect
    (and
      (on-floor ?x)
      (armempty)
      (not (holding ?x))
    )
  )

  ; po³ó¿ klocek x na y
  (:action stack
    :parameters (?x ?y)
    :precondition
    (and
      ;(clear ?x)
      (clear ?y)
;     (not(armempty))
      (holding ?x)
    )
    :effect
    (and
      (not (holding ?x))
      (armempty)
      (not (clear ?y))
      (on-top ?x ?y)
    )
  )

  ; zdejmij klocek x z y
  (:action unstack
    :parameters (?x ?y)
    :precondition
    (and
      (clear ?x)
      (on-top ?x ?y)
      (armempty)
    )
    :effect
    (and
      (holding ?x)
      (not (armempty))
      (not (on-top ?x ?y))
      (clear ?y)
    )
  )
)
